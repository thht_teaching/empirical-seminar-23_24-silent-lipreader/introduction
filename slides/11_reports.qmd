## The reports - How to style

* 12pt Non-serif font (Arial)
* 1.5 Line Spacing
* ~ 13 Pages
    * I do not count pages.

## What to submit and how

* Only submit:
    * `doc(x)`
    * `odt`
* Do **not** submit:
    * `pdf`
    * `pages`

## What to submit and how

| What                | How        |
| ------------------- | ---------- |
| Report              | Blackboard |
| SPSS/R/Jamovi Files | MS Teams   |

<div class="center-elements" style="height: 300px">
**You must submit your Statistics Files. If you do not, penalties apply!**
</div>

## What to submit and how
* The preliminary version should already be as final and complete as possible.
* The final version should incorporate the feedback that you will receive.
* [**You must use the “Track Changes” feature of your word processor!!!**]{style="color: red"}